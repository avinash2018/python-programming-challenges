'''
Write a function compress(word) that given a word word returns the string with it's letters and 
how many times they occur continously together.

For eg:

Input

a_scramble("abbs")
Output

"a1b2s1"
Explanation: 1 occurence of a, followed by 2 occurences of b, followed by 1 occurence of s

Input

compress("xxcccdex")
Output

"x2c3d1e1x1"
Explanation: 2 occurences of x, followed by 3 occurences of c, followed by 1 occurence of d, followed by 1 occurence of e, followed by 1 occurence of x
'''


def compress(word):
    count=1
    output = word[0]

    for i in range(len(word)-1):
        
        if word[i] == word[i+1]:
            count=count+1
        else:
            output=output+str(count)
            output=output+word[i+1]
            count = 1

    output = output+str(count)
    
#    print("i",str(i))
#    print("count",count)
#    print("word", word)
#    print("output",output)

    return output


if __name__ =="__main__":
    print("",compress("xxcccdex"))
