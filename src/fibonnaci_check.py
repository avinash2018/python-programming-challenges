'''
Challenge: Fibonacci Check
Create a function called check_fib(num) that checks if the given number 'num' is part of the fibonacci sequence and returns True if it is, else False

For eg:

Input

check_fib(145)
Output

False
Input

check_fib(377)
Output

True
'''
#Importing header files
from math import sqrt
"""
Iteration
Fibonacci series: 1,2,3,5,8
prev1=1
prev2=2
sum=3
prev1=2
prev2=3
sum=5
prev1=3
prev2=5
sum=8
"""
#Code starts here
def check_fib(num):
    prev1 = 1
    prev2 = 2
    sum = prev1+prev2
    if num == prev1:
        return True
    if num == prev2:
        return True
    while sum <= num:
        sum = prev1+prev2
        #print("sum,prev1,prev2",sum,prev1,prev2)
        if sum == num:
            return True
        #check for next set of numbers.
        prev1=prev2
        prev2=sum  
        #print("prev1,prev2,num", prev1,prev2,num)
    return False

if __name__ =="__main__":
    print("Is fibo",check_fib(145))
    print("Is fibo",check_fib(100))
    print("Is fibo",check_fib(377))