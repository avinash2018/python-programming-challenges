'''
Challenge: Anagram Scramble
Create a function called 'a_scramble(str_1, str_2)' that given the strings'str_1','str_2' returns True if a portion of 'str_1' characters can be scrambled to match 'str2', else return False

For eg:

Input

a_scramble("Tom Marvolo Riddle","Voldemort")
Output

True
Input

a_scramble("ticket","chat")
Output

False
'''
def a_scramble(str1, str2):
    i = 0
    count = len(str2)
    while (i < count):
        if str2[i].lower() not in str1.lower():
            return False
        i = i+1
        if i >= count:
            return True
    
def a_scramble_for_loop(str1, str2):
    for c in str2:
        if c.lower() not in str1.lower():
            return False
    return True


if __name__ =="__main__":
    print(a_scramble("ticket", "chat"))
    print(a_scramble("Tom Marvolo Riddle","Voldemort"))
    print(a_scramble_for_loop("ticket", "chat"))
    print(a_scramble_for_loop("Tom Marvolo Riddle","Voldemort"))