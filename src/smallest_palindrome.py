'''
Create a function called palindrome(num) that given a number 'num', finds the next smallest palindrome 
and returns it
'''
def palindrome(num):
    while not isPalindrome(num):
        num = num + 1     
    return num
    

'''
 A number is palindrome if it is same even after reversing.
'''       
def isPalindrome(num):
    str_num = str(num)
    strrev_num = str_num[::-1]
    if str_num == strrev_num:
        return True
    else:
        return False


if __name__ =="__main__":
    print("smallest palindrome next to 121: ", palindrome(121))
    print("smallest palindrome next to 122: ", palindrome(122))
    print("smallest palindrome next to 100", palindrome(100))
